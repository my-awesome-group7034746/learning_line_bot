<?php

/**
 * Copyright 2016 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/*
 * This polyfill of hash_equals() is a modified edition of https://github.com/indigophp/hash-compat/tree/43a19f42093a0cd2d11874dff9d891027fc42214
 *
 * Copyright (c) 2015 Indigo Development Team
 * Released under the MIT license
 * https://github.com/indigophp/hash-compat/blob/43a19f42093a0cd2d11874dff9d891027fc42214/LICENSE
 */
// Check if the function "hash_equals" does not already exist in PHP
if (!function_exists('hash_equals')) {

    // Define a constant "USE_MB_STRING" with a value indicating whether "mb_strlen" function exists or not
    defined('USE_MB_STRING') or define('USE_MB_STRING', function_exists('mb_strlen'));

    // Define the function "hash_equals" which compares two strings in a way that mitigates timing attacks
    function hash_equals($knownString, $userString)
    {
        // Define a helper function "$strlen" to calculate the length of a string, taking into account multi-byte characters if enabled
        $strlen = function ($string) {
            // If "USE_MB_STRING" is true, use "mb_strlen" to get the length of the string in 8-bit binary-safe mode
            if (USE_MB_STRING) {
                return mb_strlen($string, '8bit');
            }

            // If "USE_MB_STRING" is false, use the standard "strlen" function to get the length of the string
            return strlen($string);
        };

        // Compare the lengths of the two strings
        // If the lengths are not equal, return false (the strings are definitely not equal)
        if (($length = $strlen($knownString)) !== $strlen($userString)) {
            return false;
        }

        // Initialize a variable "$diff" to 0 to store the differences between the two strings
        $diff = 0;

        // Calculate the bitwise differences between the corresponding characters of the two strings
        // and update the "$diff" variable accordingly
        for ($i = 0; $i < $length; $i++) {
            $diff |= ord($knownString[$i]) ^ ord($userString[$i]);
        }

        // If the "$diff" variable is still 0 after comparing all characters, it means the strings are equal
        // Return true (the strings are equal), otherwise return false
        return $diff === 0;
    }
}

class LINEBotTiny
{
    private $channelAccessToken;
    private $channelSecret;

    public function __construct($channelAccessToken, $channelSecret)
    {
        $this->channelAccessToken = $channelAccessToken;
        $this->channelSecret = $channelSecret;
    }

    public function parseEvents()
    {
        // Check if the HTTP request method is not POST
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            // Set the HTTP response code to 405 (Method Not Allowed)
            http_response_code(405);
            // Log an error message indicating the method is not allowed
            error_log('Method not allowed');
            // Terminate the script execution
            exit();
        }

        // Read the request body from the input stream and store it in the variable "$entityBody"
        $entityBody = file_get_contents('php://input');

        // Check if the length of the request body is 0 (empty)
        if (strlen($entityBody) === 0) {
            // Set the HTTP response code to 400 (Bad Request)
            http_response_code(400);
            // Log an error message indicating the request body is missing
            error_log('Missing request body');
            // Terminate the script execution
            exit();
        }

        // Verify the request signature by comparing the received signature with the computed signature
        // The "hash_equals" function is used to compare the two signatures in a secure way that mitigates timing attacks
        if (!hash_equals($this->sign($entityBody), $_SERVER['HTTP_X_LINE_SIGNATURE'])) {
            // Set the HTTP response code to 400 (Bad Request)
            http_response_code(400);
            // Log an error message indicating the signature is invalid
            error_log('Invalid signature value');
            // Terminate the script execution
            exit();
        }

        // Decode the JSON data from the request body and store it in the variable "$data"
        $data = json_decode($entityBody, true);

        // Check if the "events" property exists in the decoded JSON data
        if (!isset($data['events'])) {
            // Set the HTTP response code to 400 (Bad Request)
            http_response_code(400);
            // Log an error message indicating the "events" property is missing in the request body
            error_log('Invalid request body: missing events property');
            // Terminate the script execution
            exit();
        }

        // If all checks pass, return the "events" property from the decoded JSON data
        return $data['events'];
    }

    public function replyMessage($message)
{
    // Define the headers required for the HTTP request to the LINE Bot API
    $header = array(
        'Content-Type: application/json',
        'Authorization: Bearer ' . $this->channelAccessToken,
    );

    // Create a stream context with the necessary options for the HTTP request
    $context = stream_context_create([
        'http' => [
            'ignore_errors' => true,
            'method' => 'POST', // Set the HTTP method to POST
            'header' => implode("\r\n", $header), // Concatenate headers with a carriage return and newline (CRLF)
            'content' => json_encode($message), // Convert the $message array to a JSON string
        ],
    ]);

    // Perform the HTTP request to the LINE Bot API using file_get_contents
    // The API endpoint is 'https://api.line.me/v2/bot/message/reply'
    // The response is stored in the variable $response
    $response = file_get_contents('https://api.line.me/v2/bot/message/reply', false, $context);

    // Check the HTTP response headers for the status code to verify if the request was successful
    // If the status code is not '200' (OK), log an error message
    if (strpos($http_response_header[0], '200') === false) {
        error_log('Request failed: ' . $response);
    }
}

private function sign($body)
{
    // Calculate the HMAC SHA-256 hash of the given body using the provided channelSecret as the key
    // The hash_hmac function generates a hash-based message authentication code (HMAC)
    // The hash_hmac function takes three parameters: the hash algorithm ('sha256' in this case),
    // the data to be hashed (the $body), and the secret key (the $this->channelSecret)
    // The fourth parameter (true) specifies that the output should be raw binary data
    $hash = hash_hmac('sha256', $body, $this->channelSecret, true);

    // Convert the binary hash to a base64-encoded string
    // The base64_encode function encodes binary data to a safe printable ASCII string
    $signature = base64_encode($hash);

    // Return the base64-encoded signature
    return $signature;
}
}
