<?php

// $url = 'https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw';
// // 使用 file_get_contents() 函式來抓取 API 資料
// $response = file_get_contents($url);

// // 檢查是否成功取得資料
// if ($response === false) {
//     die('無法取得 API 資料');
// }

// // 將 JSON 格式的資料解析成 PHP 陣列或物件
// $data = json_decode($response, true);
// $joke = "";
// // 獲取笑話
// if ($data['type'] === 'single') {
//     $joke = $data['joke'];
// } else {
//     $joke = $data['setup'] . ' ' . $data['delivery'];
// }

// //把笑話轉成中文
// // 設定 LibreTranslate API 網址和語言代碼
// $apiUrl = 'https://libretranslate.com/translate';
// $sourceLanguage = 'en';
// $targetLanguage = 'zh-TW';

// //設定POST請求內容
// $data = array(
//     'q' => $joke,
//     'source' => $sourceLanguage,
//     'target' => $targetLanguage
// );

// //初始化CURL
// $ch = curl_init($apiUrl);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// curl_setopt($ch, CURLOPT_POST, 1);
// curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

// //執行curl請球
// $response = curl_exec($ch);

// //處理響應
// if($response === false){
//     die("無法取得翻譯結果");
// }

// $data = json_decode($response, true);
// var_dump($data);

// //獲取翻譯後的笑話
// //$translatedJoke = $data['translatedText'];

// //輸出翻譯後的笑話
// //echo($translatedJoke);

//-------------------------------------------

$opggUrl = 'https://www.op.gg/champions/ahri'; // 替換為實際的英雄網址

// 初始化 curl
$ch = curl_init($opggUrl);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// 執行 curl 請求
$response = curl_exec($ch);

// 處理響應
if ($response === false) {
    die('無法取得網頁內容');
}else{
    $redirect_opggUrl ='https://www.op.gg' .  $response;
    $ch = curl_init($redirect_opggUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    if($response != false){
        echo($response);
    }
}