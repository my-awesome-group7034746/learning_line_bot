<?php
/**
 * Copyright 2020 GoneTone
 *
 * Line Bot
 * 範例 Example Bot (Imagemap)
 *
 * 此範例 GitHub 專案：https://github.com/GoneToneStudio/line-example-bot-tiny-php
 * 此範例教學文章：https://blog.reh.tw/archives/988
 *
 * 官方文檔：https://developers.line.biz/en/reference/messaging-api#imagemap-message
 */

/*
陣列輸出 Json
==============================
{
    "type": "imagemap",
    "baseUrl": "https://api.reh.tw/line/bot/example/assets/images/example",
    "altText": "Example imagemap"
    "baseSize": {
        "height": 1040,
        "width": 1040
    },
    "actions": [
        {
            "type": "uri",
            "linkUri": "https://github.com/GoneToneStudio/line-example-bot-tiny-php",
            "area": {
                "x": 0,
                "y": 0,
                "width": 520,
                "height": 1040
            }
        },
        {
            "type": "message",
            "text": "Hello",
            "area": {
                "x": 520,
                "y": 0,
                "width": 520,
                "height": 1040
            }
        }
    ]
}
==============================
*/
global $client, $message, $event;
if (strtolower($message['text']) == "imagemap" || $message['text'] == "圖像地圖" || $message['text'] == "圖片地圖") {
    $client->replyMessage(array(
        'replyToken' => $event['replyToken'],
        'messages' => array(
            array(
                'type' => 'imagemap', //訊息類型 (圖片地圖)
                'baseUrl' => 'https://linebot.kartahandsome.net/line/assets/images/example',
                'altText' => 'Example imagemap', //替代文字
                'baseSize' => array(
                    'height' => 1400, //圖片寬
                    'width' => 1040 //圖片高
                ),
                //在base imagemap裡面多新增video的範圍
                "video" => array(
                    //這裡和回復影片訊息一樣
                    'originalContentUrl' => 'https://linebot.kartahandsome.net/line/assets/videos/cat.mp4',
                    //然後縮圖
                    'previewImageUrl' => 'https://linebot.kartahandsome.net/line/assets/images/strange.jpg',
                    //這裡就是設定長寬大小
                    'area' => array(
                        'x'=> 520,
                        'y'=> 0,
                        'width' => 520,
                        'height' => 1040,
                    ),
                    "externalLink" => array(
                        'linkUri' => "https://www.pexels.com/search/videos/cat/",
                        'label' => "查看更多貓貓"
                    ),
                ),
                //從line developer裡面看的話就是設定點擊框框範圍會出現對應事件
                'actions' => array(
                    array(
                        'type' => 'uri', //類型 (網址)
                        'linkUri' => 'https://genrandom.com/cats/', //連結網址
                        'area' => array(
                            'x' => 0, //點擊位置 X 軸
                            'y' => 1, //點擊位置 Y 軸
                            'width' => 1033, //點擊範圍寬度
                            'height' => 699 //點擊範圍高度
                        )
                    ),
                    array(
                        'type' => 'message', //類型 (用戶發送訊息)
                        'text' => '哪裡不能亂摸，不可以瑟瑟', //發送訊息
                        'area' => array(
                            'x' => 0, //點擊位置 X 軸
                            'y' => 759, //點擊位置 Y 軸
                            'width' => 1037, //點擊範圍寬度
                            'height' => 801 //點擊範圍高度
                        )
                    )
                )
            )
        )
    ));
}
