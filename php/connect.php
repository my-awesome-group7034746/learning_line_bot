<?php
$servername = "mysql";
$username = "USER";
$password = "PASS";
$dbname = "php-app";

// 建立連線
$conn = new mysqli($servername, $username, $password, $dbname);

// 檢查連線是否成功
if ($conn->connect_error) {
    die("連線失敗: " . $conn->connect_error);
}

echo "連線成功！";