<?php
/**
 * Copyright 2020 GoneTone
 *
 * Line Bot
 * 範例 Example Bot (Audio)
 *
 * 此範例 GitHub 專案：https://github.com/GoneToneStudio/line-example-bot-tiny-php
 * 此範例教學文章：https://blog.reh.tw/archives/988
 *
 * 官方文檔：https://developers.line.biz/en/reference/messaging-api#audio-message
 */

/*
陣列輸出 Json
==============================
{
    "type": "audio",
    "originalContentUrl": "https://api.reh.tw/line/bot/example/assets/audios/example.ogg",
    "duration": 3000
}
==============================
*/
global $client, $message, $event;

// 引入 getID3 庫來取得音訊檔案資訊
require_once(dirname(dirname(__FILE__)) . '/lib/getid3/getid3.php'); 

// 如果使用者傳來 "audio"、"音頻" 或 "音樂" 
if (strtolower($message['text']) == "audio" || $message['text'] == "音頻" || $message['text'] == "音樂") {

  // 設定音訊檔案路徑和網址
  $audiofile = dirname(dirname(__FILE__)) . '/assets/audios/cat-tongue-twister.mp3';
  $audiofileurl = 'https://linebot.kartahandsome.net/line/assets/audios/cat-tongue-twister.mp3';

  // 使用 getID3 取得音訊長度
  $getID3 = new getID3;
  $file = $getID3->analyze($audiofile);
  $milliseconds = round($file['playtime_seconds'] * 1000);

  // 呼叫 Line 回覆音訊訊息
  $client->replyMessage(array(
     'replyToken' => $event['replyToken'],
     'messages' => array(
       array(
         'type' => 'audio',  
         'originalContentUrl' => $audiofileurl,
         'duration' => $milliseconds  
       )
     )
  ));

}