<?php
/**
 * Copyright 2020 GoneTone
 *
 * Line Bot
 * 範例 Example Bot (Image)
 *
 * 此範例 GitHub 專案：https://github.com/GoneToneStudio/line-example-bot-tiny-php
 * 此範例教學文章：https://blog.reh.tw/archives/988
 *
 * 官方文檔：https://developers.line.biz/en/reference/messaging-api#image-message
 */

/*
陣列輸出 Json
==============================
{
    "type": "image",
    "originalContentUrl": "https://api.reh.tw/images/gonetone/logos/icons/icon-256x256.png",
    "previewImageUrl": "https://api.reh.tw/images/gonetone/logos/icons/icon-256x256.png"
}
==============================
*/
// 如果使用者傳來的訊息文字為 "image" 或 "圖片"
if (strtolower($message['text']) == "image" || $message['text'] == "圖片") {

    // 呼叫Line Client的replyMessage方法回覆訊息
    $client->replyMessage(array(
  
      // 指定replyToken
      'replyToken' => $event['replyToken'],
  
      // 訊息內容
      'messages' => array(
  
        // 圖片訊息陣列
        array(
          
          // 訊息類型為'image'
          'type' => 'image',
          
          // 原始圖片網址
          'originalContentUrl' => 'https://linebot.kartahandsome.net/line/assets/images/catgirl.jpg',
  
          // 預覽圖片網址
          'previewImageUrl' => 'https://linebot.kartahandsome.net/line/assets/images/catgirl.jpg'
  
        )
  
      )
  
    ));
  
  }