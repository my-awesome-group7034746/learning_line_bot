<?php

require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;

$token = "02OdsHNCYDldDyPQ4WLR5PBNAfS9qsuGxyI4qnL-mE4M1FrXR1lGN91MkQHCdz9pW8W15_O11s4Y2nu4fPF206GSdWLNg";
$fileUrl = "https://b044-60-249-142-61.ngrok-free.app/audio/test.mp3";

// 初始化 curl 
$ch = curl_init();
// 設置請求選項
curl_setopt($ch, CURLOPT_URL, 'https://api.rev.ai/speechtotext/v1/jobs');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, '{"source_config":{"url":"' . $fileUrl . '"}}');
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer ' . $token,
    'Content-Type: application/json'
));

// 發送請求獲取響應
$response = curl_exec($ch);

// 關閉 curl 
curl_close($ch); 

// 解析 JSON 響應並打印

$response = (json_decode($response));

$jobId = $response->id;
// 初始化curl
$ch = curl_init();

// 設置請求選項
curl_setopt($ch, CURLOPT_URL, "https://api.rev.ai/speechtotext/v1/jobs/$jobId/transcript");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'Authorization: Bearer ' . $token,
  'Accept: application/vnd.rev.transcript.v1.0+json'  
));

// 發送請求獲取響應
$response = curl_exec($ch);

// 關閉curl會話 
curl_close($ch);

// 解析JSON響應並打印
print_r(json_decode($response));
