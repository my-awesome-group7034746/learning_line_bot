<?php
/**
 * Copyright 2020 GoneTone
 *
 * Line Bot
 * 範例 Example Bot (Video)
 *
 * 此範例 GitHub 專案：https://github.com/GoneToneStudio/line-example-bot-tiny-php
 * 此範例教學文章：https://blog.reh.tw/archives/988
 *
 * 官方文檔：https://developers.line.biz/en/reference/messaging-api#video-message
 */

/*
陣列輸出 Json
==============================
{
    "type": "video",
    "originalContentUrl": "https://api.reh.tw/line/bot/example/assets/videos/example.mp4",
    "previewImageUrl": "https://api.reh.tw/line/bot/example/assets/images/example.jpg"
}
==============================
*/
// 全域變數 
global $client, $message, $event;

// 如果使用者傳來 "video"、"視頻" 或 "影片" 
if (strtolower($message['text']) == "video" || $message['text'] == "視頻" || $message['text'] == "影片") {

  // 呼叫Line回覆影片訊息
  $client->replyMessage(array(  
   
    // 回覆的reply token
    'replyToken' => $event['replyToken'],
    
    // 訊息內容 
    'messages' => array(
    
      // 影片訊息陣列
      array( 
      
        // 訊息類型為'video'
        'type' => 'video',  
        
        // 原始影片網址 
        'originalContentUrl' => 'https://linebot.kartahandsome.net/line/assets/videos/cat.mp4',
        
        // 預覽圖片網址
        'previewImageUrl' => 'https://linebot.kartahandsome.net/line/assets/images/strange.jpg'
        
      )
    
    )
    
  ));

}
