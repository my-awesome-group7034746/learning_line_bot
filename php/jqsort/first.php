<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>JQuery UI sortable practice</title>
   <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
   <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
   <style>
      .default {
         background: #cedc98;
         border: 1px solid #DDDDDD;
         color: #333333;
      }

      .myHolder {
         background: silver;
         border: 3px;
         border-radius: 5px;
      }
   </style>
</head>

<body>
   <div class="d-flex">
      <ul id="sortable-1">
         <!-- <?php for ($i = 1; $i <= 7; $i++) : ?>
            <? echo ("<li class='default'>Product" . $i . "</li>"); ?>
         <?php endfor ?> -->
         <li class="default">Apple 1</li>
         <li class="default">Banana 2</li>
         <li class="default">Cat 3</li>
         <li class="default">Dog 4</li>
      </ul>
   </div>


</body>
<script>
   $(document).ready(function() {
      var list = $("#sortable-1");
      list.sortable({
         axis: "y",
         placeholder: "myHolder",
         update: function(e, ui){
            var items = list.find('li');
            items.each(function(idx){
               var item = $(this).text().split(" ")[0];               
               var currentNum = parseInt($(this).text().split(' ')[1]);
               $(this).text(item + " " + (idx+1));
            })
         }
      });
   });
</script>

</html>