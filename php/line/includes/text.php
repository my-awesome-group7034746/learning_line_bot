<?php
/**
 * Copyright 2020 GoneTone
 *
 * Line Bot
 * 範例 Example Bot (Text)
 *
 * 此範例 GitHub 專案：https://github.com/GoneToneStudio/line-example-bot-tiny-php
 * 此範例教學文章：https://blog.reh.tw/archives/988
 *
 * 官方文檔：https://developers.line.biz/en/reference/messaging-api#text-message
 */

/*
陣列輸出 Json
==============================
{
    "type": "text",
    "text": "Hello, world!"
}
==============================
*/

// It checks if the user’s message is either “text” or “文字” (case-insensitive). 
//If it is, then it sends a reply message to the user with the text “您好，小咪正在待主人開發，敬請期待”. The reply message is sent using the LINE Messaging API.
global $client, $message, $event;
if (strtolower($message['text']) == "text" || $message['text'] == "文字") {
    $client->replyMessage(array(
        'replyToken' => $event['replyToken'],
        'messages' => array(
            array(
                'type' => 'text', //訊息類型 (文字)
                'text' => '小咪尚被主人開發中(〃∇〃)，敬請期待 <3' //回覆訊息
            )
        )
    ));
}
